Run command
------------
php Elevator.php

Format input data
------------
Array( '1>4', '3>2', '4>1' )

Usage
------------
$inputData = ['1>4', '3>2', '4>1'];

$elevator = new Elevator();

$elevator->run($inputData);