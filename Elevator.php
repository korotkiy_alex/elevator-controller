<?php

class Elevator
{
    private $floor;  // current floor
    private $move;   // move direction UP or DOWN
    private $queue;  // queue of stops
    private $people; //

    public function __construct()
    {
        $this->floor = 1;
        $this->maxFloor = 4;
        $this->move = 'UP';
        $this->people = [];
        $this->queue = ['UP' => [], 'DOWN' => []];
    }

    public function run($inputData)
    {
        $this->prepareData($inputData);
        $this->execute();
    }

    private function prepareData($data)
    {
        foreach ($data as $key => $command) {
            $peopleQuery = $this->parseCommand($command);
            $peopleQuery['status'] = 'wait';
            $this->people[$command] = $peopleQuery;

            if ($peopleQuery['from'] < $peopleQuery['to']) {
                $this->queue['UP'][] = $peopleQuery['to'];
            } else if (end($this->queue['UP']) < $peopleQuery['from']){
                $this->queue['UP'][] = $peopleQuery['from'];
                $this->queue['DOWN'][] = $peopleQuery['to'];
            } else {
                $this->queue['DOWN'][] = $peopleQuery['to'];
            }

            if ($peopleQuery['from'] != $this->floor) {
                if ($peopleQuery['from'] < $peopleQuery['to']) {
                    $this->queue['UP'][] = $peopleQuery['from'];
                } else {
                    $this->queue['DOWN'][] = $peopleQuery['from'];
                }
            }
        }

        $this->queue['DOWN'][] = ''; // empty for finish
        $this->queue['UP'] = array_unique($this->queue['UP']);
        sort($this->queue['UP']);

        $this->queue['DOWN'] = array_unique($this->queue['DOWN']);
        $this->queue['DOWN'] = array_diff($this->queue['DOWN'], $this->queue['UP']);
        rsort($this->queue['DOWN']);
    }

    private function parseCommand($command)
    {
        $command = explode('>', $command);
        return ['from' => $command[0], 'to' => $command[1]];
    }

    private function execute()
    {
        if (count($this->queue['UP'])) {
            foreach ($this->queue['UP'] as $floor) {
                $this->moveElevator($floor);
            }

            if (count($this->queue['DOWN'])) {
                foreach ($this->queue['DOWN'] as $floor) {
                    $this->moveElevator($floor);
                }
            }
        }
    }

    private function moveElevator($nextFloor = null)
    {
        $left = 0;
        $came = 0;

        if ($this->floor == $this->maxFloor) {
            $this->move = 'DOWN';
        }

        if ($this->floor != $nextFloor) {
            echo "Floor " . $this->floor . "\n";
            echo "door open" . "\n";

            foreach ($this->people as &$people) {
                if ($people['status'] == 'in' && $people['to'] == $this->floor) {
                    $left++;
                    $people['status'] = 'left';
                }

                if ($people['status'] == 'wait' && $people['from'] == $this->floor) {
                    $came++;
                    $people['status'] = 'in';
                }
            }
            echo "persons left - " . $left . "\n";
            echo "persons came - " . $came . "\n";

            if ($nextFloor) {
                echo "next floor is " . $nextFloor . "\n";
                echo "door close" . "\n";
                echo "moving " . $this->move . "\n";
                echo "------------------------" . "\n";

                $this->floor = $nextFloor;
            } else {
                echo "door close" . "\n";
                echo "------------------------" . "\n";
            }
        }
    }
}

$inputData = ['1>4', '3>2', '4>1'];
//$inputData = ['1>3', '3>2', '4>1'];
//$inputData = ['1>3', '4>2'];

$elevator = new Elevator();
$elevator->run($inputData);